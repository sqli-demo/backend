package com.sujan.sqlidemoserver.util;

import com.sujan.sqlidemoserver.exception.CommonException;
import com.sujan.sqlidemoserver.model.LoginRequest;
import org.owasp.encoder.Encode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
    public static boolean validateLogin(LoginRequest request) {
        if (isValidEmail(request.getEmail()))
            return true;
        else
            throw new CommonException("Invalid Email!");
    }

    private static boolean isValidEmail(String email) {
        String emailRegx = "^[A-Za-z0-9+_.-]+@([A-Za-z0-9.-]+).([A-Za-z]{2,4})$";
        Pattern pattern = Pattern.compile(emailRegx);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static void validatePostAndSanitize(String post) {
        if (post == null)
            throw new CommonException("Cannot be empty.");
        if (!isValidLength(post))
            throw new CommonException("Post can only be less than 150 words.");
        if (!isValidContent(post))
            throw new CommonException("Post can only contain letters, numbers and symbols(. , ! ?)");
    }

    private static boolean isValidContent(String post) {
        String postRegx = "^[A-Za-z0-9\\s.,!?]*$";
        Pattern pattern = Pattern.compile(postRegx);
        Matcher matcher = pattern.matcher(post);
        return matcher.matches();
    }

    private static boolean isValidLength(String post) {
        String[] words = post.split("\\s+");
        return words.length <= 150;
    }
}
