package com.sujan.sqlidemoserver.util;

import com.sujan.sqlidemoserver.model.Posts;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import static java.util.logging.Level.INFO;
import static java.util.logging.Level.WARNING;

public class DBManager {
    private static final String DRIVER = "com.mysql.jdbc.Driver";
    private static final String USER = "root";
    private static final String PASS = "";
    private static final String URL = "jdbc:mysql://localhost:3306/assessment3";
    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    public boolean isValidLogin(String email, String password) throws IOException {
        Statement stat;
        Connection connection;
        String checkQ = "select * from users where email = '" + email + "' and password = '" + password + "';";
        try {
            Class.forName(DRIVER);
            connection = getConnection();
            stat = connection.createStatement();

            LOGGER.log(INFO, "Query Executing: "+ checkQ );
            ResultSet resultSet = stat.executeQuery(checkQ);
            if (resultSet.next()) {
                LOGGER.log(INFO, "Valid Login credentials");
                return true;
            } else {
                LOGGER.log(INFO, "Invalid Login credentials");
                return false;
            }
        } catch (Exception e) {
            LOGGER.log(WARNING, e.getMessage() );
            return false;
        }
    }

    public List<Posts> listPosts(){
        Statement stat;
        Connection connection;
        String query = "select * from posts;";
        List<Posts> posts = new ArrayList<>();
        try {
            Class.forName(DRIVER);
            connection = getConnection();
            stat = connection.createStatement();

            LOGGER.log(INFO, "Query Executing: "+ query );
            ResultSet resultSet = stat.executeQuery(query);
            while(resultSet.next()){
                Posts post = new Posts();
                post.setPostBy(resultSet.getString("post_by"));
                post.setDescription(resultSet.getString("description"));
                post.setCreatedDate(LocalDate.parse(resultSet.getString("created_date")));
                post.setId(resultSet.getString("id"));
                posts.add(post);
            }
        } catch (Exception e) {
            LOGGER.log(WARNING, e.getMessage());
        }
        return posts;
    }

    private static Connection getConnection() throws SQLException {
        return DriverManager.getConnection(URL, USER, PASS);
    }

    public Posts addPost(String post) {
        String id = UUID.randomUUID().toString();
        LocalDate now = LocalDate.now();
        Statement stat;
        Connection connection;
        String checkQ = "INSERT INTO posts (id, post_by, created_date, description) VALUES ('"
                +id+"','admin','"
                +now+"', '"
                +post+"');";
        try {
            Class.forName(DRIVER);
            connection = getConnection();
            stat = connection.createStatement();
            LOGGER.log(INFO, "Query Executing: "+ checkQ );
            int res = stat.executeUpdate(checkQ);
            if (res == 1){
                return new Posts(id, post, "admin", now);
            }
            return new Posts();
        } catch (Exception e) {
            LOGGER.log(WARNING, "Posting failed: "+e.getMessage() );
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }
}
