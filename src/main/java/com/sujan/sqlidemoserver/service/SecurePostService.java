package com.sujan.sqlidemoserver.service;

import com.sujan.sqlidemoserver.model.Posts;
import com.sujan.sqlidemoserver.repositories.PostRepository;
import com.sujan.sqlidemoserver.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.UUID;
import java.util.logging.Logger;

@Service
public class SecurePostService {
    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());
    @Autowired
    private PostRepository postRepository;

    public Posts post(String post) {
        Validator.validatePostAndSanitize(post);
        Posts saved = postRepository.save(new Posts(UUID.randomUUID().toString(), post, "admin", LocalDate.now()));
        return saved;
    }
}
