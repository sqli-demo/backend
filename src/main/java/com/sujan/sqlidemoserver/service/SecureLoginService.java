package com.sujan.sqlidemoserver.service;

import com.sujan.sqlidemoserver.exception.CommonException;
import com.sujan.sqlidemoserver.model.LoginRequest;
import com.sujan.sqlidemoserver.model.Users;
import com.sujan.sqlidemoserver.repositories.UserRepository;
import com.sujan.sqlidemoserver.util.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.logging.Level;
import java.util.logging.Logger;

@Service
public class SecureLoginService {
    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());
    @Autowired
    private UserRepository userRepository;

    public Users login(LoginRequest request) {
        Users user = null;
        if (Validator.validateLogin(request)){
            user = userRepository.findByEmailAndPassword(request.getEmail(), request.getPassword());
            if (user == null)
                throw new CommonException("Email or password invalid.");
        }
        return user;
    }
}
