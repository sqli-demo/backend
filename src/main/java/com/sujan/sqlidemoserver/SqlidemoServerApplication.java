package com.sujan.sqlidemoserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SqlidemoServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SqlidemoServerApplication.class, args);
	}

}
