package com.sujan.sqlidemoserver.repositories;

import com.sujan.sqlidemoserver.model.Posts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PostRepository extends JpaRepository<Posts, String>{
}