package com.sujan.sqlidemoserver.repositories;

import com.sujan.sqlidemoserver.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<Users, String>{
    Users findByEmailAndPassword(String email, String password);
}