package com.sujan.sqlidemoserver.controller;

import com.sujan.sqlidemoserver.model.Posts;
import com.sujan.sqlidemoserver.service.SecurePostService;
import com.sujan.sqlidemoserver.util.DBManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.logging.Logger;

@RestController
@CrossOrigin
@RequestMapping("api/post")
public class PostController {
    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    @Autowired
    private SecurePostService postService;

    @GetMapping("/list")
    public ResponseEntity<Object> list() {
        DBManager manager = new DBManager();
        List<Posts> posts = manager.listPosts();
        return new ResponseEntity<>(posts, HttpStatus.OK);
    }

    @PostMapping("/new")
    public ResponseEntity<Object> post(@RequestBody String post) {
        DBManager manager = new DBManager();
        Posts posts = manager.addPost(post);
        return new ResponseEntity<>(posts, HttpStatus.OK);
    }

    @PostMapping("/securely/new")
    public ResponseEntity<Object> postSecurely(@RequestBody String post) {
        Posts response = postService.post(post);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
}
