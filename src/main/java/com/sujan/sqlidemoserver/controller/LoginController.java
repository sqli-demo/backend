package com.sujan.sqlidemoserver.controller;

import com.sujan.sqlidemoserver.model.LoginRequest;
import com.sujan.sqlidemoserver.model.Users;
import com.sujan.sqlidemoserver.service.SecureLoginService;
import com.sujan.sqlidemoserver.util.DBManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

@RestController
@CrossOrigin
@RequestMapping("api/login")
public class LoginController {
    private final Logger LOGGER = Logger.getLogger(this.getClass().getName());

    @Autowired
    private SecureLoginService secureLoginService;

    @GetMapping("/test")
    public ResponseEntity<Object> test() {
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    @PostMapping("/insecure")
    public ResponseEntity<Object> loginInsecure(@RequestBody LoginRequest request) {
        LOGGER.log(Level.INFO, "Loging in User: "+ request.getEmail());
        DBManager manager = new DBManager();
        boolean result = false;
        try {
            result = manager.isValidLogin(request.getEmail(), request.getPassword());

        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return new ResponseEntity<>( result , HttpStatus.OK);
    }

    @PostMapping("/secure")
    public ResponseEntity<Object> loginSecure(@RequestBody LoginRequest request) {
        LOGGER.log(Level.INFO, "Loging in User: "+ request.getEmail());
        Users login = secureLoginService.login(request);
        return new ResponseEntity<>( login , HttpStatus.OK);
    }


}
