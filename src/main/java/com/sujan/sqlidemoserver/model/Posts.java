package com.sujan.sqlidemoserver.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.*;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Entity
public class Posts {
    @Id
    private String id;
    private String description;
    private String postBy;
    private LocalDate createdDate;
}
